import { createApp } from 'vue'
import store from './store/'
import route from './route'
import App from './App.vue'

const app = createApp(App)

app

.use(store)
.use(route)
.mount('#app')
