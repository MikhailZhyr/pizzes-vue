import axios from "axios";
import { createStore } from "vuex";

// import cart from "./cart";




const store = {
  //   namespaced: true,
  //   modules: {
  //     cart,
  //   },
  state: {
    categories: [
      {
        id: 0,
        name: "Все",
      },
      {
        id: 1,
        name: "Мясные",
      },
      {
        id: 2,
        name: "Вегетарианские",
      },
      {
        id: 3,
        name: "Гриль",
      },
      {
        id: 4,
        name: "Острые",
      },
      {
        id: 5,
        name: "Закрытые",
      },
    ],
    sort: [
      {
        id: 0,
        name: "Популярные",
        sort: "rating",
        order: "asc",
      },
      {
        id: 1,
        name: "Не популярные",
        sort: "rating",
        order: "desc",
      },
      {
        id: 2,
        name: "цене (по возрастанию)",
        sort: "price",
        order: "asc",
      },
      {
        id: 3,
        name: "цене (по убыванию)",
        sort: "price",
        order: "desc",
      },
    ],
    salSort: "",
    categoryActive: "",
    products: [],
    product: {},
    cart: [],
    page: 1,
    limit: 4,
    isActiveBtnGetMore: false,
    selCategory: 0,
    isActionSucces: false,
    isValidityId: true
  },
  getters: {
    products: (state) => state.products,
    isActiveBtnGetMore: (state) => state.isActiveBtnGetMore,
    categoryActive: (state) => state.categoryActive,
    selCategory: (state) => state.selCategory,
    cart:state => state.cart,
    product:state=> state.product,
    quantityCartAll:state => state.cart.map(el => el.quantity).reduce((old, newQy) => old + newQy, 0),
    priceCartAll:state => state.cart.map(el => el.product.price * el.quantity).reduce((old, newQy) => old + newQy, 0),
    salSort: (state) => state.salSort,
    isValidityId: state => state.isValidityId,
    isActionSucces: (state) => state.isActionSucces,
    textTypePizzes: () => item => item.type === 1 ? 'тонкое' : 'традиционное',
    calcElementsCart:(state) => id => state.cart.filter(el => el.product.id == id).map(item => item.quantity).reduce((old, newQy) => old + newQy, 0)
  },
  mutations: {
    setProducts(state, products) {
      products.forEach((item) => {
        (item.size = item.sizes[0]), (item.type = item.types[0]);
      });
      state.products = products;
      if (state.limit > products.length) {
        state.isActiveBtnGetMore = true;
      } else if (state.limit == products.length) {
        state.isActiveBtnGetMore = false;
      }
    },
    storeCart(state){
      localStorage.getItem('cart') ? state.cart = JSON.parse(localStorage.getItem('cart')) : ''
    },
    addCartProduct(state, product) {
      let isValibile = true;
      if (state.cart.length) {
        state.cart.filter((p) => {
          if (
            p.product.id === product.id &
            p.size === product.size &
            p.type === product.type
          ) {

            p.quantity = p.quantity + 1;
            isValibile = false;
          }
        });
      }
      if (isValibile) state.cart.push({
        id: Date.now(),
        product,
        quantity: 1,
        size: product.size,
        type: product.type,
      });
      localStorage.setItem('cart', JSON.stringify(state.cart))
    },

    setProductLearnMore(state, newProducts) {
      newProducts.forEach((item) => {
        (item.size = item.sizes[0]), (item.type = item.types[0]);
      });
      state.products = [...state.products, ...newProducts];
      if (state.limit > newProducts.length) {
        state.isActiveBtnGetMore = true;
      } else if (state.limit == newProducts.length) {
        state.isActiveBtnGetMore = false;
      }
    },
    pageCount(state) {
      return (state.page = state.page + 1);
    },
    setCategories(state, category) {
  
      state.selCategory = category;
   
      state.page = 1;
    },
    setSelSort(state, sorting) {
      state.page = 1;
      state.salSort = sorting;
    },
    activeCategory(state, category) {
      category
        ? (state.categoryActive = category)
        : (state.categoryActive = "");
    },
    setProductSingle(state, productSingle){
      productSingle.size = productSingle.sizes[0]
      productSingle.type = productSingle.types[0]
  
      state.product = productSingle
    },
    isActionSuccesTrue(state) {
      state.isActionSucces = true;
    },
    isActionSuccesFalse(state) {
      state.isActionSucces = false;
    },
    choisSizeProduct(state, argument) {
    
      argument.product.size = argument.size;
    },
    choisTypeProduct(state, argument){
    
      argument.product.type = argument.type;
    },
    quantityProductAddoCart(state, item){
        item.quantity = item.quantity + 1
        localStorage.setItem('cart', JSON.stringify(state.cart))
    },
    quantityProductRemoveoCart(state, item) {
        if(item.quantity > 1) item.quantity = item.quantity - 1

        localStorage.setItem('cart', JSON.stringify(state.cart))
    },
    deliteCartProduct(state, product){
       let result = confirm('Вы хотите убрать пиццу из корзины?')
       if(result) {
        state.cart = state.cart.filter(item => item != product)   
       }
       localStorage.setItem('cart', JSON.stringify(state.cart))
    },
    deliteAllProductsToCart(state) {
        let result = confirm('Вы хотите очистить корзину?')
        if(result) {
            state.cart = []
        }
        localStorage.setItem('cart', JSON.stringify(state.cart))
    },
    isValidityIdNotFound(state, bol){
      state.isValidityId = bol
    }
  },
  actions: {
     getProducts({ state, commit }) {
      commit("setCategories", state.categories[0]);
      commit("setSelSort", state.sort[0]);
      commit("activeCategory");
      commit("isActionSuccesTrue");
      commit("storeCart")
      axios
        .get(
          `https://62f0d64a57311485d137d1ad.mockapi.io/items?page=${
            state.page
          }&limit=${state.limit}&category=${
            !state.categoryActive.id ? "" : state.categoryActive.id
          }&sortBy=${state.salSort.sort}&order=${state.salSort.order}`
        )
        .then((res) => {
          return commit("setProducts", res.data);
        })
        .finally(() => commit("isActionSuccesFalse"));
    },
     getGetMoreProduct({ state, commit }) {
      commit("pageCount");
      axios
        .get(
          `https://62f0d64a57311485d137d1ad.mockapi.io/items?page=${
            state.page
          }&limit=${state.limit}&category=${
            !state.categoryActive.id ? "" : state.categoryActive.id
          }&sortBy=${state.salSort.sort}&order=${state.salSort.order}`
        )
        .then((res) => {
          return commit("setProductLearnMore", res.data);
        });
    },
     getCategoriesProduct({ state, commit }, idCategories) {
      commit("setCategories", idCategories);
      commit("activeCategory", idCategories);
      commit("isActionSuccesTrue");
      axios
        .get(
          `https://62f0d64a57311485d137d1ad.mockapi.io/items?page=${
            state.page
          }&limit=${state.limit}&category=${
            !state.categoryActive.id ? "" : state.categoryActive.id
          }&sortBy=${state.salSort.sort}&order=${state.salSort.order}`
        )
        .then((res) => {
          return commit("setProducts", res.data);
        })
        .finally(() => commit("isActionSuccesFalse"));
    },
     getSortProduct({ state, commit }, sorting) {
      commit("setSelSort", sorting);
      commit("isActionSuccesTrue");
      axios
        .get(
          `https://62f0d64a57311485d137d1ad.mockapi.io/items?page=${
            state.page
          }&limit=${state.limit}&category=${
            !state.categoryActive.id ? "" : state.categoryActive.id
          }&sortBy=${state.salSort.sort}&order=${state.salSort.order}`
        )
        .then((res) => {
          return commit("setProducts", res.data);
        })
        .finally(() => commit("isActionSuccesFalse"));
    },
    getSingleProduct({commit}, id){
      commit("isActionSuccesTrue");
      axios.get(`https://62f0d64a57311485d137d1ad.mockapi.io/items/${id}`)
      .then(res => {
        commit("isValidityIdNotFound", true)
        commit("setProductSingle", res.data)
      } )
      .catch((err) => {
        console.log(err)
        commit("isValidityIdNotFound", false)
      })
      .finally(() => commit("isActionSuccesFalse"));
    },
    addToCart({ commit }, product) {
      commit("addCartProduct", product);
    },
    getChoiseProduct({ commit }, argument) {
      commit("choisSizeProduct", argument);
    },
    getTypeProduct({commit}, argument){
      commit("choisTypeProduct", argument);
    },
    quantityAddToCartAction({commit}, item){
        commit("quantityProductAddoCart", item)
    },
    quantityRemoveToCartAction({commit}, item){
        commit("quantityProductRemoveoCart", item)
    },
    deliteCartProductAction({commit}, product){
        commit('deliteCartProduct', product)
    },
    deliteAllProductsToCartAction({commit}){
        commit('deliteAllProductsToCart')
    },
    getCartProduct({commit}){
      commit("storeCart")
    }
  },
};




// items?page=1&limit=4&sortBy=rating&order=desc items?page=1&limit=4&sortBy=rating&order=asc
export default createStore(store);
