import { createRouter, createWebHistory } from "vue-router";
import ContentProduct from "@/components/ContentProduct";
import CartProduct from "@/components/CartProduct";
import ProductSingle from "@/components/ProductSingle";
import NotFound from "@/components/NotFound"
const  routes = [
  {
    name: 'main',
    path: "/",
    component: ContentProduct,
  },
  {
    name: 'cart',
    path: "/cart",
    component: CartProduct,
  },
  {
    name: 'single-product',
    path: "/pizza/:id",
    component: ProductSingle,
  },
  {
    name: 'notfound',
    path:"/:pathMatch(.*)*",
    component: NotFound
  }
]
// import MovieItem from "@/components/MovieItem"
export default createRouter({
  mode: 'history',
  routes,
  history: createWebHistory(),
});
